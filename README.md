# Maskrcnn Grape Detection project

Проект по детекцию и сегментации винограда на изображениях 
с использованием MaskRCNN. Использовался датасет ([WGISD](https://github.com/thsant/wgisd): Embrapa Wine Grape Instance Segmentation Dataset). Данный проект включает в себя тренировку и оценку модели. К тому же представлено два подхода визуализации детекции и сегментации винограда по снимкам. 

## Установка

1. Склонировать репозиторий проекта

`$ git clone git@gitlab.com:main_projects1/maskrcnn_grape_detection.git`

2. Скачать датасет https://github.com/thsant/wgisd

3. Склонировать версию библиотеки MaskRCNN, совместимую с tensorflow2.0:

`$ git clone -b tensorflow2.0 https://github.com/leekunhee/Mask_RCNN.git`

4. Произвести установку ([Python 3.10.6](https://www.python.org/downloads/macos/)), Tensorflow 2.10.0, Keras, 2.10.0.

Необходимо осуществить работу ускорителя GPU, для этого необходимо установить ([cuDNN](https://developer.nvidia.com/rdp/cudnn-archive)) и ([CUDA](https://developer.nvidia.com/cuda-toolkit-archive)).
Совместимость версий при установки учесть согласно документации https://www.tensorflow.org/install/source#gpu.
Пример инструкции по установке ([в статье](https://medium.com/@ashkan.abbasi/quick-guide-for-installing-python-tensorflow-and-pycharm-on-windows-ed99ddd9598)).

5. Установить необходимые Python-библиотеки:

`$ pip install -r req.txt`


## Запуск
1. Запустить скрипт подготовки тренировочного, валидационного и тестового датасетов:

`$ python build_train_dataset.py`

`$ python build_test_dataset.py`


2. Обучить модель CNN с помощью графического процессора с указанием пути к датасету:

`$ python grape.py train --dataset=/path/to/dataset/ --weights=coco`

Описание параметров запуска доступно по команде:

$ python grape.py -h

```console
usage: grape.py [-h] [--dataset /path/to/balloon/dataset/] --weights /path/to/weights.h5 [--logs /path/to/logs/] [--image path or URL to image] <command>

Train Mask R-CNN to detect balloons.

positional arguments:
  <command>             'train' or 'evaluate' or 'splash' or 'detect'

options:
  -h, --help            show this help message and exit
  --dataset /path/to/balloon/dataset/
                        Directory of the Balloon dataset
  --weights /path/to/weights.h5
                        Path to weights .h5 file or 'coco'
  --logs /path/to/logs/
                        Logs and checkpoints directory (default=logs/)
  --image path or URL to image
                        Image to apply the color splash effect on
```

В результате работы скрипта в директории logs в корне проекта будут появляться веса модели по окончанию обучения каждой эпохи. Сохраненные на 6 эпохах веса (mask_rcnn_grape_0006.h5) представлены в директории colab.


## Структура проекта

Тогда структура проекта должна выглядеть следующим образом:

* **/colab** - набор скриптов и файлов для детекции и сегментации винограда в Google Colab:
  * **grape.py** - скрипт для обучения и валидации модели,
  * **Grapes_Solution.ipynb** - jupyter notebook с запуском кода,
  * **mask_rcnn_grape_0006.h5** - веса обученной модели.
  
* **/dataset** - датасет:
  * **/train** - тренировочный,
  * **/test** - тестовый,
  * **/val** - валидационный.

* **build_train_dataset.py** - скрипт для создания тренировочного и валидационного датасетов,
* **build_test_dataset.py** - скрипт для создания тестового датасета,
* **grape.py** - скрипт для обучения, оценки модели и получения результатов детекции и сегментации,
* **example** - результаты детекции и сегментации по снимкам.


## Примеры выполнения

Результат распознавания в задаче детекции:

![img.png](example/detect.png)

Результат распознавания в задачи сегментации:

![img.png](example/splash.png)


#### Подробная инструкция по установке и запуску кода в Google Colab представлена в ipynb и README файлах в директории colab.